import React, { Component } from 'react'

class Table extends Component {
  constructor(props) {
    super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
    this.state = { //state is by default an object
      showSubmitButton:false,
      selectedStudents : null,
      clickedSubmit : false,
      students: [
        { id: 1, name: 'Wasif', age: 21, email: ['wasif@email.com', 'wasif2@email.com', 'wasif3@email.com'], selected : false },
        { id: 2, name: 'Ali', age: 19, email: ['ali@email.com', 'ali@email2.com', 'ali@email3.com'] ,selected : false},
        { id: 3, name: 'Saad', age: 16, email: ['saad@email.com', 'saad2@email.com', 'saad3@email.com'] ,selected : false},
        { id: 4, name: 'Asad', age: 25, email: ['asad@email.com', 'asad2@email.com', 'asad3@email.com'] ,selected : false}
      ]
    }
  }
  selectUser(){
    this.state.selectedStudents = null;
    let id = arguments[0];
    let o  = this.state.students;
    o.map(item => {
      if (item.id == id) {
            item.selected = !item.selected;
      }
    })
    this.setState({
      students: o
    });
     this.state.showSubmitButton = false;
     let selectedStudents = this.state.students.filter(item=>{
       if(item.selected == true){
         return item;
       }
     })
     if(selectedStudents.length > 0){
      this.state.showSubmitButton = true;
     }
     this.state.selectedStudents = selectedStudents;
  }
  
  deleteUser(event) {
    let id = arguments[0];
    this.setState({
      students: this.state.students.filter(item => {
        if (item.id !== id) {
          return item;
        }
      })
    });
  }
  renderTableData() {
    return this.state.students.map((student, index) => {
      const { id, name, age, email ,selected} = student //destructuring
      return (
        <tr key={id}>
          <td><input type="checkbox"  checked={selected} onChange={() => { this.selectUser(id)}}/></td>
          <td>{id}</td>
          <td>{name}</td>
          <td>{age}</td>
          <td><select>{email.map((team) => <option value={team}>{team}</option>)}></select></td>
          <td><button onClick={() => { this.deleteUser(id) }}>Delete</button></td>
        </tr>
      )
    })
  }
  compareBy(key) {
    return function (a, b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    };
  }  
  submitfrom(){
     this.setState({clickedSubmit:true});
    console.log(this.state.selectedStudents);

  }
  sortBy(key) {
    let arrayCopy = [...this.state.students];
    arrayCopy.sort(this.compareBy(key));
    this.setState({students: arrayCopy});
  }
  renderSelectedTableData(){
    return this.state.selectedStudents.map((student, index) => {
      const { id, name, age, email ,selected} = student //destructuring
      return (
        <tr key={id}>
          <td>{id}</td>
          <td>{name}</td>
          <td>{age}</td>
        </tr>
      )
    })
  }
  render() {
    return (
      <div>
        <h1 id='title'>React Dynamic Table</h1>
        <table id='students'>
          <thead>
            <td onClick={() => this.sortBy('id')} >ID</td>
            <td onClick={() => this.sortBy('name')}>name</td>
            <td onClick={() => this.sortBy('age')}>Age</td> 
          </thead>
          <tbody>
            {this.renderTableData()}
          </tbody>
        </table>
        {this.state.showSubmitButton ? <button onClick={() => this.submitfrom()}>submit</button> : !null}
        <table id='selectedstudents'>
          <tbody>
          {this.state.selectedStudents && this.state.clickedSubmit ? this.renderSelectedTableData() : null} 
          </tbody>
        </table>
      </div>
    )
  }

}

export default Table 