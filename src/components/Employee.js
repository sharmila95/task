import React, { Component } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Styles/Employee.css'

function searchingFor(term){
    return function(x){
        return x.text.toLowerCase().includes(term.toLowerCase()) || !term; 
    }
}

class Employee extends React.Component {
    
    constructor(props) {
        super(props) 
        this.state = {
            showSubmitButton:false,
            selectedEmployees : null,
            clickedSubmit : false,
            items: [],
            term: ''
        }
        
        this.searchHandler = this.searchHandler.bind(this);
        this.componentWillMount = this.componentWillMount.bind(this);
        this.selectUser = this.selectUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.renderTableData = this.renderTableData.bind(this);
        this.compareBy = this.compareBy.bind(this);
        this.sortBy = this.sortBy.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.renderSelectedTableData = this.renderSelectedTableData.bind(this);
    }

    componentWillMount() {
        axios.get('http://localhost:5555/items')
        .then(res => {
            this.setState({items: res.data });
        })            
    }
  
    selectUser(){
        this.state.selectedEmployees = null;
        this.state.clickedSubmit = false;
        let id = arguments[0];
        let o  = this.state.items;
        o.map(item => {
            if (item.id == id) {
                    item.selected = !item.selected;
            }
        })
        this.setState({
            items: o
        });
        this.state.showSubmitButton = false;
            let selectedEmployees = this.state.items.filter(item=>{                
                if(item.selected ==true){
                    return item;
                }
        })
        if(selectedEmployees.length > 0){
            this.state.showSubmitButton = true;
        }
        this.state.selectedEmployees = selectedEmployees;
    }

    deleteUser(event) {
        let id = arguments[0];
        this.setState({
            items: this.state.items.filter(item => {
                if (item.id !== id) {
                    return item;                    
                }
            })
        });
    }

    handleChange(event){
        this.setState({
            value: event.target.value
        })
    }
        
    renderTableData() {
        return this.state.items.filter(searchingFor(this.state.term)).map((item, index) => {
            const { id, text, Tags, selected } = item 
            return (
            <tr key={id}>
                <td><input type="checkbox" checked={selected} onChange={() => { this.selectUser(id)}}/></td>
                <td>{id}</td>
                <td>{text}</td>
                <td><select  onChange={this.handleChange}>{Tags.map((course) => <option value={course} >{course}</option>)}</select></td>
                <td><button onClick={() => { this.deleteUser(id) }} className="btn btn-danger">Delete</button></td>
            </tr>
            )
        })
    }

    compareBy(key) {
        return function (a, b) {
          if (a[key] < b[key]) return -1;
          if (a[key] > b[key]) return 1;
          return 0;
        };
    }  
    
    sortBy(key) {
        let arrayCopy = [...this.state.items];
        arrayCopy.sort(this.compareBy(key));
        this.setState({items: arrayCopy});
    }

    handleSubmit(){
        this.setState({clickedSubmit:true});
    }

    renderSelectedTableData(){
        return this.state.selectedEmployees.map((data, index) => {
            const { id, text} = data 
            return (
            <tr key={id}>
                <td>{id}</td>
                <td>{text}</td>
            </tr>
            )
        })
    }

    searchHandler(event){
        this.setState({ term: event.target.value})
    }

    render() {
        return (
            <div className="container">                
                <div className="tableCont"> 
                    <h1 style={{textAlign:"center"}}>Employee Details</h1>
                    <div className="tableMain">
                        <form className="mx-auto">
                            <label>Search: </label>&nbsp;&nbsp;
                            <input type="text"  
                                    onChange={this.searchHandler}
                                    value={this.state.term}
                            />
                        </form>
                        <br></br>
                        <table id='items'>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th onClick={() => this.sortBy('id')}>ID</th>
                                    <th onClick={() => this.sortBy('text')}>Name</th>
                                    <th onClick={() => this.sortBy('Tags')}>Courses</th>
                                    <th></th> 
                                </tr>                        
                            </thead>
                            <tbody>
                                {this.renderTableData()}
                            </tbody>
                        </table><br></br>
                        {this.state.showSubmitButton ? <button className="btn btn-primary" onClick={() => {this.handleSubmit()}}>submit</button> : null}
                    </div>
                    <table id='selectedemployees'>
                        <tbody>
                            {this.state.selectedEmployees  && this.state.clickedSubmit ? this.renderSelectedTableData() : null } 
                        </tbody>
                    </table>
                </div>
            </div>
            )
        }
    }

export default Employee;